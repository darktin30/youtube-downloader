# Youtube Downloader

Simple shell script using yt-dlp to download single youtube videos or entire youtube playlists!

## Dependencies

### On machine
yt-dlp
python3-brotli

### Pip3
pycryptodomex

## Install

Clone repo:  

```
git clone https://gitlab.com/darktin30/youtube-downloader.git
```

Make executable:

```
cd youtube-downloader
chmod +x downloader.sh
```

Install dependencies listed above!

## Usage

See help message of script

```
./downloader.sh -h
```
