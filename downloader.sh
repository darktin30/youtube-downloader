#!/usr/bin/zsh

ARCHIVE_FILE="'already-downloaded-videos.txt'"
FORMAT="'ba'"
DOWNLOAD_FOLDER="./downloads"
FOLDER_NAME="%(playlist)s"
FILE_NAME="%(title)s.%(ext)s"

download_playlist() {
	command="yt-dlp "
	command+="-f ${FORMAT} "
	command+="--download-archive ${ARCHIVE_FILE} "
	command+="-x --audio-format mp3 --add-metadata "
	command+="'${1}' "
	command+="-o '${DOWNLOAD_FOLDER}/${FOLDER_NAME}/${FILE_NAME}'"

	eval $command
}

download_song() {
	command="yt-dlp "
	command+="-f ${FORMAT} "
	command+="--download-archive ${ARCHIVE_FILE} "
	command+="-x --audio-format mp3 --add-metadata "
	command+="--no-playlist "
	command+="'${1}' "
	command+="-o '${DOWNLOAD_FOLDER}/${FILE_NAME}'"

	eval $command
}

search_and_download() {
	command="yt-dlp "
	command+="-f ${FORMAT} "
	command+="--download-archive ${ARCHIVE_FILE} "
	command+="-x --audio-format mp3 --add-metadata "
	command+="ytsearch1:\"${1}\" "
	command+="-o '${DOWNLOAD_FOLDER}/${FILE_NAME}'"

	eval $command
}

batch_mode() {
	if [ -p /dev/stdin ]; then
		while IFS= read line; do
			./$1 -o "${DOWNLOAD_FOLDER}" $2 "$line"
		done
	else
		echo "For batch mode, please pass input via pipe to stdin!"
	fi
}

display_help() {
    echo "Usage: $0 [optional_flag [optional_var]] command_flag command_var" >&2
    echo
    echo "Optional flags:"
    echo "  -o, --outputfolder 	Specify output folder path"
    echo "  -b, --batch        	Run command in batch mode, expecting file pipe as input."
    echo "			Has to be last of optional flags!"
    echo
    echo "Command flags:"
    echo "  -l, --link		Download single song from link"
    echo "  -p, --playlist	Download all songs from link"
    echo "  -s, --search       	Search and download best hit"
    echo
    echo "			Following example shows usecase with text file full of search queries"
    echo "			e.g. $> cat my-songs.txt | ./downloader.sh -o './nice-songs' --batch -s"
    echo
    echo "  -h, --help		Print this message"
    echo
    exit 1
}

while :
do
	case "$1" in

		-o | --outputfolder)
			echo "Output folder is $2"
			DOWNLOAD_FOLDER=$2
			shift 2
			;;
		-b | --batch)
			echo "Batch mode activated!"
			batch_mode $0 $2
			exit 1
			;;
		-p | --playlist)
			echo "Downloading all songs in $2"
			download_playlist $2
			exit 1
			;;
	
		-l | --link)
			echo "Downloading song pointed to by $2"
			download_song $2
			exit 1
			;;

		-s | --search)
			echo "Searching youtube for $2"
			search_and_download $2
			exit 1
			;;

		-h | --help)
			display_help
			;;

		--) # End of all options
			shift
			break
			;;

		-*)
			echo "Error: Unknown option: $1" >&2
			echo
			display_help
			;;

		*)
			display_help
			exit 1
			;;
	esac
done

